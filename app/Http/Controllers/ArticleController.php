<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;

class ArticleController extends Controller
{
    
    public function index()
    {
        //
        
        $articles = Article::paginate(3);
        return view('articles.list')->with('articles',$articles);
    }

    public function create()
    {
        //
        return view('articles.create');
    }


    public function store(Request $request)
    {
        //
         
        $article = new Article([
            'title' => $request->get('title'),
            'author' => $request->get('author'),
            'content' => $request->get('content'),
            'img_name' => basename($request->file('img')->store('public/images'))
        ]);

        
     

        $article->save();

        return redirect()->route('article.index');
    }


    public function search () {

        $text = $_GET['value'];
        $articles = Article::where('title','LIKE',"%$text%")->paginate(3);
        return view('articles.list')->with('articles',$articles);
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
