<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'name' => 'tien vu',
            'email' => 'tieuviem@gmail.com',
            'password' => '12345678',
            'role' => 'admin'
        ]);
    }
}
